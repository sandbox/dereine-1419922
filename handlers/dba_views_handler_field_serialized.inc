<?php

/**
 * Dba field handler to show serialize content.
 *
 * @ingroup views_field_handlers
 */
class dba_views_handler_field_serialized extends views_handler_field_serialized {
  function query() {
    $this->field_alias = $this->field;
  }
}
