<?php

/**
 * Base dba field handler.
 *
 * @ingroup views_field_handlers
 */
class dba_views_handler_field extends views_handler_field {
  function query() {
    $this->field_alias = $this->field;
  }
}
