<?php

/**
 * Dba views query plugin.
 */
class dba_views_plugin_query_dba extends views_plugin_query {
  public $dba_handler;

  function option_definition() {
    $options = parent::option_definition();
    $options['path'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#default_value' => $this->options['path'],
    );
  }

  function build(&$view) {
    parent::build($view);

    $this->view = $view;
    $view->init_pager();
    $this->dba_handler = dba_open($this->options['path'], 'r');
  }

  function query($get_count = FALSE) {
    $this->pager->query();
  }


  function execute(&$view) {
    $count = 0;

    while ($count <= 10) {
      if ($count == 0) {
        $key = dba_firstkey($this->dba_handler);
      }
      else {
        $key = dba_nextkey($this->dba_handler);
      }
      if ($key !== FALSE) {
        $stream = dba_fetch($key, $this->dba_handler);
        $item = unserialize($stream);
        $view->result[] = (object) $item;
      }
      else {
        break;
      }
      $count++;
    }
  }
}
