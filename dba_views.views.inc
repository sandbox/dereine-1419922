<?php

/**
 * Implements hook_views_data().
 */
function dba_views_views_data() {
  $data = array();
  $data['dba']['table']['group'] = t('Dba');
  $data['dba']['table']['base'] = array(
    'field' => 'cid',
    'title' => t('Dba'),
    'help' => t('TODO'),
    'query class' => 'dba',
  );

  // cid
  $data['dba']['cid'] = array(
    'title' => t('Cid'),
    'help' => t('Cache ID'),
    'field' => array(
      'handler' => 'dba_views_handler_field',
    ),
  );

  $data['dba']['data'] = array(
    'title' => t('Data'),
    'help' => t('Data of the cache'),
    'field' => array(
      'handler' => 'dba_views_handler_field_serialized',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_plugins().
 */
function dba_views_views_plugins() {
  // example code here
  $plugins = array();
  $plugins['query']['dba'] = array(
    'title' => t('Dba storage'),
    'help' => t('TODO.'),
    'handler' => 'dba_views_plugin_query_dba'
  );

  return $plugins;
}
